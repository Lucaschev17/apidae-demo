<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>APIDAE DEMO</title>
</head>
<body>
    <?php 
    $apiKey = '5bSry5xh';
    $projetId = '5023';
    
    $requete = array();
    $requete['apiKey'] = $apiKey;
    $requete['projetId'] = $projetId;
    $requete['selectionIds'] = [98706];
    $requete['responseFields'] = ["nom", "localisation.geolocalisation.geoJson", "informations.moyensCommunication.coordonnees"];
    $url = 'https://api.apidae-tourisme.com/api/v002/recherche/list-objets-touristiques';
    $url .= '?';
    $url .= 'query='.urlencode(json_encode($requete));
    
    $response = file_get_contents($url);
    $response = json_decode($response);
    echo "<pre>";
    print_r($response);
    echo "</pre>";
    ?>
</body>
</html>